import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kaadodetoreeningu/splash_screen.page.dart';
import 'package:kaadodetoreeningu/translation.page.dart';
import 'home.page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
//      initialRoute: '/',
//      routes: {
////        '/': (context) => SplashScreenPage(),
//        '/': (context) => HomePage(),
//        '/TranslationPage': (context) => TranslationPage(),
//      },
    );
  }
}


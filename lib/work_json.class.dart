class WordJson{
  List data; //

  List getTitles(){
    return ["Naturia"];
  }

  List getJson(){
     return [
       [
         ['kurage', "água viva", "海月", "くらげ"],
         ["neko", "gato", "猫", "ねこ"],
         ["tsuki", "lua", "月", "つき"],
       ]
     ];
  }

  List getRandomWord(String listName,int randWord, int randType){
    List completeWord;
    switch(listName.toLowerCase()){
      case 'easy':
        completeWord = getListWordEasy()[ randWord  ];
        break;
      case 'medium':
        completeWord = getListWordMedium()[ randWord  ];
        break;
      case 'hard':
        completeWord = getListWordHard()[ randWord  ];
        break;
    }
    return completeWord;

//    return "${word[0].toUpperCase()}${word.substring(1)}";
  }

  List getListWordEasy(){
    return [
      ["Haha","mãe","母"],
      ["Chichi","pai","父"],
      ["Kon'nichiwa","olá","こんにちは"],
      ["Gogo","tarde","午後"],
      ["Asa","manhã", "朝"],
      ["Hi","dia","日"],
    ];
  }

  List getListWordMedium() {
    return [
      ["Ototo","irmão mais novo","弟"],
      ["Imoto","irmã mais nova","妹"],
      ["Yoru","noite","夜"],
      ["Ohayo gozaimasu","bom dia","おはようございます"],
      ["Oyasuminasai","boa noite (durma bem)","おやすみなさい"]
    ];
  }

  List getListWordHard() {
    return [
      ["Kanojo wa watashi no haha desu","esta é minha mãe","彼女は私の母です"],
      ["Kare wa watashi no chichi desu","este é meu pai","彼は私の父です"],
      ["Kanojo wa watashi no imoto desu","esta é minha irmã","彼女は私の妹です"],
      ["Kare wa watashi no ototo desu","este é meu irmão","彼は私の弟です"],
    ];
  }
}

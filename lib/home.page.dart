import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kaadodetoreeningu/translation.page.dart';
import 'package:kaadodetoreeningu/work_json.class.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text("Japa Tegami"),
        centerTitle: true,
      ),

      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.86,
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

              Padding(
                  padding: EdgeInsets.only(top:10, left:5, right:5),
                  child: Card(
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: RichText(
                          text: TextSpan(
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black54
                              ),
                              children: [
                                TextSpan(text:
                                "Olá, este é o aplicativo "
                                ),

                                TextSpan(text:
                                "Japa Tegami",
                                    style: TextStyle(color: Colors.black)
                                ),

                                TextSpan(text:
                                " para treino na linguagem japonesa "
                                    "através de memorização de frases e palavras. "
                                    "Escolha entre os níveis "
                                ),

                                TextSpan(text:
                                "fácil, ",
                                    style: TextStyle(color: Colors.green)
                                ),
                                TextSpan(text:
                                "moderado ",
                                    style: TextStyle(color: Colors.indigoAccent)
                                ),
                                TextSpan(text:
                                "e ",
                                ),
                                TextSpan(text:
                                "difícil ",
                                    style: TextStyle(color: Colors.black87)
                                ),
                                TextSpan(text:
                                "para começar."
                                ),
                              ]
                          ),
                        ),
                      )
                  )
              ),

              SizedBox(height: 50),

              _createBottonsSelect(context),

//              difficultyButton("簡単", Colors.green, context, "easy"),
//
//              SizedBox(height: 15),
//
//              difficultyButton("中級", Colors.indigoAccent, context, "medium"),
//
//              SizedBox(height: 15),
//
//              difficultyButton("難しい", Colors.black87, context, "hard"),

              Expanded(
                child:Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(left: 5,right: 5),
                    child: Card(
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: RichText(
                          text: TextSpan(
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.black54
                            ),
                            children: [
                              TextSpan(text:
                              "By: "
                              ),

                              TextSpan(text:
                              "juliolimareis@gmail.com",
                                  style: TextStyle(color: Colors.orange)
                              ),

                              TextSpan(text:
                              " e "
                              ),

                              TextSpan(text:
                              "barbarafonseca95@gmail.com",
                                  style: TextStyle(color: Colors.deepPurpleAccent)
                              ),

                            ]
                          ),
                        ),
                      )
                    )
                  ),
                )
              )

            ],
          ),
        ),
      )

    );
  }

  Widget _createBottonsSelect(context){
    List titles = new WordJson().getTitles();
    List wordjson = new WordJson().getJson();

    return SizedBox(
//        height: MediaQuery.of(context).size.height * 0.75,
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: EdgeInsets.all(10.0),
            itemCount: wordjson.length,
            itemBuilder: (context, index){
              return difficultyButton(titles[index], Colors.indigoAccent, context, "medium");
            }

        )
    );
  }

  Widget difficultyButton(String text, Color color, context, String difficulty){
    return Center(
      child: ButtonTheme(
        minWidth: 200.0,
        child:RaisedButton(
          padding: EdgeInsets.only(top: 10, bottom: 11),
          color: color,
          textColor: Colors.white,
          child: Text(
            text,
            style: TextStyle(
                fontSize: 20,
            ),
          ),
          onPressed: (){

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) =>
                  TranslationPage(difficulty: difficulty)
              ),
            );

          },
        )
      ),
    );
  }

}

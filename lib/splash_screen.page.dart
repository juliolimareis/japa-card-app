import 'package:flutter/material.dart';
import 'package:kaadodetoreeningu/home.page.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashScreenPage extends StatefulWidget {

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  Widget build(BuildContext context) {
    return _introScreen();
  }
}

Widget _introScreen() {
  return Stack(
    children: <Widget>[
      SplashScreen(
        seconds: 3,
//        gradientBackground: LinearGradient(
//          begin: Alignment.topRight,
//          end: Alignment.bottomLeft,
//          colors: [
//            Colors.white,
////            Colors.black
//          ],
//        ),
        navigateAfterSeconds: HomePage(),
        loaderColor: Colors.transparent,
      ),
       Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
//            verticalDirection: ,
            children: <Widget>[

              Image.asset(
                "assets/icon/icon.png",
                width: 200,
                height: 200,
              ),

              SizedBox(height: 25),

              Text(
                "Japa Tegami",
                style: TextStyle(
                  fontSize: 26,
                  color: Colors.black87,
                  decoration: TextDecoration.none
                ),

              )

            ],
          )
        ),
    ],
  );
}
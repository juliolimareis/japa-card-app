import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kaadodetoreeningu/work_json.class.dart';
import 'dart:math';

class TranslationPage extends StatefulWidget {
  final String difficulty;

  TranslationPage({this.difficulty});

  @override
  _TranslationPageState createState() => _TranslationPageState();
}

class _TranslationPageState extends State<TranslationPage> {
  Random random = new Random();
  WordJson wordJson = new WordJson();
  String word;
  String wordTranslation;
  int randWord;
  int randType;
  Color textColor;
  Color cardColor;
  String mainButtonName;
  bool onMainButtonName;
  String textBar;
  Color textBarColor;
  List completeWord;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    drawWord();
  }

  void drawWord(){ //difficulty
    mainButtonName = "Tradução";
    onMainButtonName = false;
    textColor = Colors.black87;
    cardColor = Colors.white70;
    randType = random.nextInt(2)+1;
    wordTranslation = "";
    switch( widget.difficulty.toLowerCase() ){
      case "easy":
        textBar = "簡単";
        textBarColor = Colors.green;
        randWord = random.nextInt( wordJson.getListWordEasy().length );
        break;
      case "medium":
        textBar = "中級";
        textBarColor = Colors.indigoAccent;
        randWord = random.nextInt( wordJson.getListWordMedium().length );
        break;
      case "hard":
        textBar = "難しい";
        textBarColor = Colors.white;
        randWord = random.nextInt( wordJson.getListWordHard().length );
        break;
      default:
        randWord = random.nextInt( wordJson.getListWordEasy().length );
        break;
    }
    completeWord = wordJson.getRandomWord( widget.difficulty, randWord, randType);
    word = completeWord[randType];
    word = "${word[0].toUpperCase()}${word.substring(1)}";
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: RichText(
            text: TextSpan(
              children: [
                TextSpan(text:"Japa Tegami - ", style: TextStyle(color: Colors.white)),
                TextSpan(text:textBar, style: TextStyle(color: textBarColor))
              ],
              style: TextStyle(
                  fontSize: 20,
              )
            ),
          ),
          centerTitle: true,
        ),
        body: ListView(
          padding: EdgeInsets.only(top:10),
          children: <Widget>[

            card(word, true, Colors.black87, Colors.white70),

            card(wordTranslation, onMainButtonName, Colors.white70, Colors.blueAccent),

            Padding(
              padding: EdgeInsets.only(top:10),
            ),

            Center(
              child: RaisedButton(
                padding: EdgeInsets.only(left:40, right: 40, top: 10, bottom: 11),
                color: Colors.black87,
                textColor: Colors.white,
                child: Text(
                  mainButtonName,
                  style: TextStyle(
                      fontSize: 20
                  ),

                ),
                onPressed: (){
                  setState(() {
                    if( onMainButtonName ){
                      drawWord();
                    }else{
                      wordTranslation = completeWord[0];
                      onMainButtonName = true;
                      mainButtonName = "Reiniciar";
                    }

                  });

                },
              ),
            )


          ],
        )
    );
  }

  Widget card(String text, bool visible, textColor, cardColor){
//    if(!visible){
//      return Container();
//    }
    return Container(
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.95,
          height: MediaQuery.of(context).size.height * 0.35,
          child: Card(
            color: visible ? cardColor : Colors.transparent,
            child: ListView(
              padding: EdgeInsets.all(20),
              children: <Widget>[

                Center(
                  child: Text(
                    text,
                    style: TextStyle(
                        fontSize: 30,
                        color: textColor //textColor
                    ),
                  ),
                )

              ],
            ),
          ),
        ),
      ),
    );
  }

}